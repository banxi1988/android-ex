package xiyili.widgets;

import android.text.Editable;
import android.widget.TextView;

/**
 * Created by banxi on 14-4-29.
 * View的一些实用函数
 */
public class Views {
    public static String str(TextView textView) {
        if (textView == null) {
            return null;
        }
        CharSequence text = textView.getText();
        if (text == null) {
            return  null;
        }
        return text.toString().trim();
    }
}
