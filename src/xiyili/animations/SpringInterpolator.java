package xiyili.animations;

import android.view.animation.Interpolator;

/**
 * Created by banxi on 14-5-16.
 */
public class SpringInterpolator implements Interpolator {
    private long mLastTimeMillis;

    // storage for the current and prior physics state while integration is occurring
  private static class PhysicsState {
    double position;
    double velocity;
  }
      // all physics simulation objects are final and reused in each processing pass
  private final PhysicsState mCurrentState = new PhysicsState();
  private final PhysicsState mPreviousState = new PhysicsState();
  private final PhysicsState mTempState = new PhysicsState();
      // maximum amount of time to simulate per physics iteration in seconds (4 frames at 60 FPS)
  private static final double MAX_DELTA_TIME_SEC = 0.064;
  // fixed timestep to use in the physics solver in seconds
  private static final double SOLVER_TIMESTEP_SEC = 0.001;

  private SpringConfig mSpringConfig;
      // thresholds for determining when the spring is at rest
  private double mRestSpeedThreshold = 0.005;
  private double mDisplacementFromRestThreshold = 0.005;
  private double mTimeAccumulator = 0;
  private boolean mOvershootClampingEnabled;
  //
  private double mEndValue = 1f;
  private double mStartValue = 0f;

  public SpringInterpolator() {
        mSpringConfig = SpringConfig.defaultConfig;
    }

    @Override
  public float getInterpolation(float input) {
      long currentTimeMillis = System.currentTimeMillis();
       if (mLastTimeMillis == -1) {
           mLastTimeMillis = currentTimeMillis - 1;
       }
       long ellapsedMillis = currentTimeMillis - mLastTimeMillis;
       mLastTimeMillis = currentTimeMillis;
       advance(currentTimeMillis/1000.0,ellapsedMillis/1000.0);
       return (float) getCurrentValue();
  }
      /**
   * Get the current
   * @return current value
   */
  public double getCurrentValue() {
    return mCurrentState.position;
  }

      /**
   * set the velocity on the spring in pixels per second
   * @return the spring for chaining
   */
  private SpringInterpolator setVelocity(double velocity) {
    mCurrentState.velocity = velocity;
    return this;
  }
      /**
   * get the threshold of displacement from rest below which the spring should be considered at rest
   * @return displacement to consider resting below
   */
  public double getRestDisplacementThreshold() {
    return mDisplacementFromRestThreshold;
  }

  /**
   * Force the spring to clamp at its end value to avoid overshooting the target value.
   * @param overshootClampingEnabled whether or not to enable overshoot clamping
   * @return the spring for chaining
   */
  public SpringInterpolator setOvershootClampingEnabled(boolean overshootClampingEnabled) {
    mOvershootClampingEnabled = overshootClampingEnabled;
    return this;
  }

  /**
   * Check if overshoot clamping is enabled.
   * @return is overshoot clamping enabled
   */
  public boolean isOvershootClampingEnabled() {
    return mOvershootClampingEnabled;
  }



  /**
   * Check if the spring is overshooting beyond its target.
   * @return true if the spring is overshooting its target
   */
  public boolean isOvershooting() {
    return (mStartValue < mEndValue && getCurrentValue() > mEndValue) ||
        (mStartValue > mEndValue && getCurrentValue() < mEndValue);
  }
      /**
   * get the displacement of the springs current value from its rest value.
   * @return the distance displaced by
   */
  public double getCurrentDisplacementDistance() {
    return getDisplacementDistanceForState(mCurrentState);
  }

  /**
   * get the displacement from rest for a given physics state
   * @param state the state to measure from
   * @return the distance displaced by
   */
  private double getDisplacementDistanceForState(PhysicsState state) {
    return Math.abs(mEndValue - state.position);
  }

      /**
   * check if the current state is at rest
   * @return is the spring at rest
   */
  public boolean isAtRest() {
    return Math.abs(mCurrentState.velocity) <= mRestSpeedThreshold &&
        getDisplacementDistanceForState(mCurrentState) <= mDisplacementFromRestThreshold;
  }

     /**
   * advance the physics simulation in SOLVER_TIMESTEP_SEC sized chunks to fulfill the required
   * realTimeDelta.
   * The math is inlined inside the loop since it made a huge performance impact when there are
   * several springs being advanced.
   * @param time clock time
   * @param realDeltaTime clock drift
   */
  void advance(double time, double realDeltaTime) {
    // clamp the amount of realTime to simulate to avoid stuttering in the UI. We should be able
    // to catch up in a subsequent advance if necessary.
    double adjustedDeltaTime = realDeltaTime;
    if (realDeltaTime > MAX_DELTA_TIME_SEC) {
      adjustedDeltaTime = MAX_DELTA_TIME_SEC;
    }

    /* begin debug
    long startTime = System.currentTimeMillis();
    int iterations = 0;
    end debug */

    mTimeAccumulator += adjustedDeltaTime;

    double tension = mSpringConfig.tension;
    double friction = mSpringConfig.friction;

    double position = mCurrentState.position;
    double velocity = mCurrentState.velocity;
    double tempPosition = mTempState.position;
    double tempVelocity = mTempState.velocity;

    double aVelocity, aAcceleration;
    double bVelocity, bAcceleration;
    double cVelocity, cAcceleration;
    double dVelocity, dAcceleration;

    double dxdt, dvdt;

    // iterate over the true time
    while (mTimeAccumulator >= SOLVER_TIMESTEP_SEC) {
      /* begin debug
      iterations++;
      end debug */
      mTimeAccumulator -= SOLVER_TIMESTEP_SEC;

      if (mTimeAccumulator < SOLVER_TIMESTEP_SEC) {
        // This will be the last iteration. Remember the previous state in case we need to
        // interpolate
        mPreviousState.position = position;
        mPreviousState.velocity = velocity;
      }

      // Perform an RK4 integration to provide better detection of the acceleration curve via
      // sampling of Euler integrations at 4 intervals feeding each derivative into the calculation
      // of the next and taking a weighted sum of the 4 derivatives as the final output.

      // This math was inlined since it made for big performance improvements when advancing several
      // springs in one pass of the BaseSpringSystem.

      // The initial derivative is based on the current velocity and the calculated acceleration
      aVelocity = velocity;
      aAcceleration = (tension * (mEndValue - tempPosition)) - friction * velocity;

      // Calculate the next derivatives starting with the last derivative and integrating over the
      // timestep
      tempPosition = position + aVelocity * SOLVER_TIMESTEP_SEC * 0.5;
      tempVelocity = velocity + aAcceleration * SOLVER_TIMESTEP_SEC * 0.5;
      bVelocity = tempVelocity;
      bAcceleration = (tension * (mEndValue - tempPosition)) - friction * tempVelocity;

      tempPosition = position + bVelocity * SOLVER_TIMESTEP_SEC * 0.5;
      tempVelocity = velocity + bAcceleration * SOLVER_TIMESTEP_SEC * 0.5;
      cVelocity = tempVelocity;
      cAcceleration = (tension * (mEndValue - tempPosition)) - friction * tempVelocity;

      tempPosition = position + cVelocity * SOLVER_TIMESTEP_SEC;
      tempVelocity = velocity + cAcceleration * SOLVER_TIMESTEP_SEC;
      dVelocity = tempVelocity;
      dAcceleration = (tension * (mEndValue - tempPosition)) - friction * tempVelocity;

      // Take the weighted sum of the 4 derivatives as the final output.
      dxdt = 1.0/6.0 * (aVelocity + 2.0 * (bVelocity + cVelocity) + dVelocity);
      dvdt = 1.0/6.0 * (aAcceleration + 2.0 * (bAcceleration + cAcceleration) + dAcceleration);

      position += dxdt * SOLVER_TIMESTEP_SEC;
      velocity += dvdt * SOLVER_TIMESTEP_SEC;
    }

    mTempState.position = tempPosition;
    mTempState.velocity = tempVelocity;

    mCurrentState.position = position;
    mCurrentState.velocity = velocity;

    if (mTimeAccumulator > 0) {
      interpolate(mTimeAccumulator / SOLVER_TIMESTEP_SEC);
    }

    // End the spring immediately if it is overshooting and overshoot clamping is enabled.
    // Also make sure that if the spring was considered within a resting threshold that it's now
    // snapped to its end value.
    if (isAtRest() || (mOvershootClampingEnabled && isOvershooting())) {
      // Don't call setCurrentValue because that forces a call to onSpringUpdate
      mStartValue = mEndValue;
      mCurrentState.position = mEndValue;
      setVelocity(0);
//      isAtRest = true;
    }

    /* begin debug
    long endTime = System.currentTimeMillis();
    long elapsedMillis = endTime - startTime;
    Log.d(TAG,
        "iterations:" + iterations +
            " iterationTime:" + elapsedMillis +
            " position:" + mCurrentState.position +
            " velocity:" + mCurrentState.velocity +
            " realDeltaTime:" + realDeltaTime +
            " adjustedDeltaTime:" + adjustedDeltaTime +
            " isAtRest:" + isAtRest +
            " wasAtRest:" + mWasAtRest);
    end debug */

    // NB: do these checks outside the loop so all listeners are properly notified of the state
    //     transition
//    boolean notifyActivate = false;
//    if (mWasAtRest) {
//      mWasAtRest = false;
//      notifyActivate = true;
//    }
//    boolean notifyAtRest = false;
//    if (isAtRest) {
//      mWasAtRest = true;
//      notifyAtRest = true;
//    }
//    for (SpringListener listener : mListeners) {
//      // starting to move
//      if (notifyActivate) {
//        listener.onSpringActivate(this);
//      }
//
//      // updated
//      listener.onSpringUpdate(this);
//
//      // coming to rest
//      if (notifyAtRest) {
//        listener.onSpringAtRest(this);
//      }
//    }
  }


  /**
   * linear interpolation between the previous and current physics state based on the amount of
   * timestep remaining after processing the rendering delta time in timestep sized chunks.
   * @param alpha from 0 to 1, where 0 is the previous state, 1 is the current state
   */
  private void interpolate(double alpha) {
    mCurrentState.position = mCurrentState.position * alpha + mPreviousState.position *(1-alpha);
    mCurrentState.velocity = mCurrentState.velocity * alpha + mPreviousState.velocity *(1-alpha);
  }

}
