package xiyili.ui;

import android.app.Activity;
import android.content.Context;
import android.support.v4.text.TextUtilsCompat;
import android.text.TextUtils;
import android.widget.Toast;

import xiyili.G;


/**
 * 
 * 一个显示Toast的实用工具类
 * 
 * @author banxi1988
 * 
 */
public class Toasts {

	private static Toast sToast;

	public static void showLong(String message) {
		show(message, Toast.LENGTH_LONG);
	}

	public static void showShort( String message) {
		show( message, Toast.LENGTH_SHORT);
	}


	public static void showLong(int textId) {
		show( textId, Toast.LENGTH_LONG);
	}


	public static void showShort( int textId) {
		show(textId, Toast.LENGTH_SHORT);
	}

	/**
	 * 调用的最基本的Toast show方法。
	 * 指定的显示时间显示一条Toast消息。
	 *
	 * @param text
	 *            消息
	 * @param duration
	 *            显示时间
	 */
	public static void show(String text, int duration) {
		cancel();
        Context context = G.ctxOrNull();
        if (context == null) {
            return;
        }
        sToast = Toast.makeText(context, text, duration);
		// toast.setGravity(Gravity.CENTER, 0, 80);
		sToast.show();
	}

	/**
	 * 使用指定的资源ID，指定的显示时间显示一条Toast消息。
	 *
	 * @param textId
	 *            字符串资源ID
	 * @param duration
	 *            显示时间
	 */
	public static void show(int textId, int duration) {
        Context context = G.ctxOrNull();
        if (context == null) {
            return;
        }
		show(context.getString(textId), duration);
	}


	public static void showFailure( int textId) {
		showShort( textId);

	}

	public static void showFailure( String text) {
		showShort( text);
	}

	public static void showLongFailure( String text) {
		showLong( text);
	}



	public static void showSuccess( String text) {
		showShort(text);
	}

	public static void showSuccess( int textId) {
		showShort(textId);
	}

    // 下面是老的调用方法



	public static void showLong(Context context, String message) {
		show(context, message, Toast.LENGTH_LONG);
	}

	/**
	 * 相对较短时间显示一条Toast消息
	 * 
	 * @param context
	 * @param message
	 *            消息文本
	 */
	public static void showShort(Context context, String message) {
		show(context, message, Toast.LENGTH_SHORT);
	}

	/**
	 * 使用文本资源ID显示时间比较长的Toast消息。
	 * 
	 * @see {@link #showLong(Context, String)}
	 * @param context
	 * @param textId
	 *            文本资源ID
	 */
	public static void showLong(Context context, int textId) {
		show(context, textId, Toast.LENGTH_LONG);
	}

	/**
	 * 使用文本资源ID显示时间比较短的Toast消息。
	 * 
	 * @see {@link #showShort(Context, String)}
	 * @param context
	 * @param textId
	 *            文本资源ID
	 */
	public static void showShort(Context context, int textId) {
		show(context, textId, Toast.LENGTH_SHORT);
	}

	/**
	 * 调用的最基本的Toast show方法。
	 * 指定的显示时间显示一条Toast消息。
	 * 
	 * @param context
	 * @param text
	 *            消息
	 * @param duration
	 *            显示时间
	 */
	public static void show(Context context, String text, int duration) {
		cancel();
		sToast = Toast.makeText(context, text, duration);
		// toast.setGravity(Gravity.CENTER, 0, 80);
		sToast.show();
	}

	/**
	 * 使用指定的资源ID，指定的显示时间显示一条Toast消息。
	 * 
	 * @param context
	 * @param textId
	 *            字符串资源ID
	 * @param duration
	 *            显示时间
	 */
	public static void show(Context context, int textId, int duration) {
		show(context, context.getString(textId), duration);
	}

	/**
	 * 使用文件资源ID显示一个表示成功的Toast消息。
	 * 
	 * @see {@link #showSuccess(Context, String)}
	 * @param context
	 * @param textId
	 *            文本资源ID
	 */
	public static void showSuccess(Context context, int textId) {
		showSuccess(context, context.getString(textId));
	}

	/**
	 * 使用文件资源ID显示一个表示失败的Toast消息。
	 * 
	 * @see {@link #showFailure(Context, String)}
	 * @param context
	 * @param textId
	 *            文本资源ID
	 */
	public static void showFailure(Context context, int textId) {
		showShort(context, textId);
//		showIconToast(context, context.getString(textId),
//				R.drawable.ic_failure, R.color.black_dark_text, Toast.LENGTH_LONG);
	}

	/**
	 * 显示一个表示失败的Toast类型消息，使用一个红色打叉的图标，红色字体， Toast显示时间为 {@link Toast#LENGTH_LONG}
	 * 
	 * @param context
	 *            一般在Activity中使用{@link Activity#getApplicationContext()}来获得此参数
	 * @param text
	 *            需要显示的文本。
	 */
	public static void showFailure(Context context, String text) {
		showShort(context, text);
	}
	
	public static void showLongFailure(Context context, String text) {
		showLong(context, text);
	}
	

	/**
	 * 显示一个自定义的Toast消息。
	 * 
	 * @param context
	 * @param textId
	 *            文本资源ID
	 * @param iconId
	 *            图标资源ID
	 * @param colorId
	 *            文本颜色资源ID
	 * @param duration
	 *            显示时长
	 */
	public static void showIconToast(Context context, String textId,
			int iconId, int colorId, int duration) {
		cancel();
//		LayoutInflater inflater = (LayoutInflater) context
//				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		View layout = inflater.inflate(R.layout.toast, null);
//		((TextView) layout).setText(textId);
//		((TextView) layout).setTextColor(context.getResources().getColor(
//				colorId));
//		((TextView) layout).setCompoundDrawablesWithIntrinsicBounds(iconId, 0,
//				0, 0);
//		sToast = new Toast(context);
//		// toast.setGravity(Gravity.CENTER_VERTICAL, 0, -100);
//		sToast.setDuration(duration);
//		sToast.setView(layout);
//		sToast.show();
	}
	
	/**
	 * 使用一个自定义布局的View来显示Toast
	 * @param context
	 * @param text
	 */
	public static void displayFailure(Context context, String text){
		cancel();
//		LayoutInflater inflater = (LayoutInflater) context
//				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		View layout = inflater.inflate(com.xiyili.support.v8.widget.R.layout.common_fail_toast, null);
//		TextView textView = (TextView) layout.findViewById(R.id.common_toast_fail_tip);
//		textView.setText(text);
//		int duration = Toast.LENGTH_SHORT;
//		sToast = new Toast(context);
//		sToast.setDuration(duration);
//		sToast.setView(layout);
//		sToast.show();
	}

	public static void cancel() {
		if(sToast != null) {
			sToast.cancel();
			sToast = null;
		}
	}

	/**
	 * 显示一个表示成功的Toast类型消息，使用一个绿色打勾的图标，字体颜色为holo_blue Toast显示时间为
	 * {@link Toast#LENGTH_LONG}
	 * 
	 * @param context
	 *            一般在Activity中使用{@link Activity#getApplicationContext()}来获得此参数
	 * @param text
	 *            需要显示的文本。
	 */
	public static void showSuccess(Context context, String text) {
		// showIconToast(context, text, R.drawable.ic_success,
		// R.color.holo_blue,
		// Toast.LENGTH_LONG);
		showShort(context, text);

	}

    public static void networkError() {
        showFailure("网络错误,请检查网络后重试,或联系客服");
    }

    public static void showAuthExpires() {
        showFailure("亲,授权已经过期,请重新登录");

    }

    public static void show(boolean ok, String message) {
        if (ok) {
           if(!TextUtils.isEmpty(message)) showSuccess(message);
        }else{
           if(!TextUtils.isEmpty(message)) showFailure(message);
        }
    }
}
