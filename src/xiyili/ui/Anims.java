package xiyili.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;

import xiyili.compat.Env;

/**
 * Created by banxi on 14-4-29.
 *
 */
public class Anims {
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static void shake(View view) {
        int duration = 1000;
        Interpolator interpolator = new CycleInterpolator(7);
        if (Env.hasICS) {
            view.animate()
                    .translationXBy(10)
                    .setInterpolator(interpolator)
                    .setDuration(duration)
                    .start();
        }else{
            TranslateAnimation anim = new TranslateAnimation(0,10,0,0);
            anim.setDuration(duration);
            anim.setInterpolator(interpolator);
            view.startAnimation(anim);
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static void hide(final View view) {
        if (Env.hasICS) {
            view.setAlpha(1f);
            view.setTranslationY(0f);
            view.animate()
                    .alpha(0f)
                    .translationY(view.getHeight())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            view.setVisibility(View.GONE);
                        }
                    }).start();
        }else{
            view.setVisibility(View.GONE);
        }
    }

        /**
     * Show the label using an animation
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static void show(View view) {
        view.setVisibility(View.VISIBLE);
        if (Env.hasICS) {
            view.setAlpha(0f);
            view.setTranslationY(view.getHeight());
            view.animate()
                    .alpha(1f)
                    .translationY(0f)
                    .setListener(null).start();
        }
    }



    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static PropertyValuesHolder pvhFadeIn() {
        return PropertyValuesHolder.ofFloat(View.ALPHA, 0f, 1f);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static PropertyValuesHolder pvhAlpha(float... values) {
        return PropertyValuesHolder.ofFloat(View.ALPHA,values);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static PropertyValuesHolder pvhFadeOut() {
        return PropertyValuesHolder.ofFloat(View.ALPHA, 1f, 0f);
    }



    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static PropertyValuesHolder pvhScaleX(float from,float to) {
        return PropertyValuesHolder.ofFloat(View.SCALE_X, from,to);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static PropertyValuesHolder pvhScaleXIn() {
        return PropertyValuesHolder.ofFloat(View.SCALE_X, 0f,1f);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static PropertyValuesHolder pvhScaleYIn() {
        return PropertyValuesHolder.ofFloat(View.SCALE_Y, 0f,1f);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static PropertyValuesHolder pvhScaleY(float from,float to) {
        return PropertyValuesHolder.ofFloat(View.SCALE_Y, from,to);
    }


    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static PropertyValuesHolder pvhSlideToOriginal() {
        return PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0f);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static ObjectAnimator animSlideDownFadeIn(View view,int startTY) {
        view.setAlpha(0f);
        view.setTranslationY(startTY);
        return ObjectAnimator.ofPropertyValuesHolder(view, pvhFadeIn(), pvhSlideToOriginal());
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static ObjectAnimator animSlideDown(View view,int startTY) {
        view.setTranslationY(startTY);
        return ObjectAnimator.ofPropertyValuesHolder(view, pvhSlideToOriginal());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static ObjectAnimator animFadeIn(View view) {
        view.setAlpha(0f);
        return ObjectAnimator.ofPropertyValuesHolder(view, pvhFadeIn());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static ObjectAnimator animScaleFadeIn(View view) {
        view.setAlpha(0f);
        return ObjectAnimator.ofPropertyValuesHolder(view, pvhFadeIn(),pvhScaleXIn(),pvhScaleYIn());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static ObjectAnimator anim(View view,PropertyValuesHolder... values) {
        return  ObjectAnimator.ofPropertyValuesHolder(view,values);
    }


    public static AccelerateInterpolator accelerate() {
        return new AccelerateInterpolator();
    }
    public static AccelerateInterpolator accelerate(float factor) {
        return new AccelerateInterpolator(factor);
    }

    public static DecelerateInterpolator decelerate() {
        return new DecelerateInterpolator();
    }
    public static DecelerateInterpolator decelerate(float factor) {
        return new DecelerateInterpolator(factor);
    }

    public static AccelerateDecelerateInterpolator accDec() {
        return new AccelerateDecelerateInterpolator();
    }
    public static BounceInterpolator bounce() {
        return  new BounceInterpolator();
    }

    public static AnticipateInterpolator aniticipate() {
        return new AnticipateInterpolator();
    }

    /**
     * 此插值器一般用于移动动画,来达到例如,移到前先向后退的效果.
     * 例如,人们开始向前冲向,一般是身体先后退一下.
     * @param tension
     * @return
     */
    public static AnticipateInterpolator aniticipate(float tension) {
        return new AnticipateInterpolator(tension);
    }

    /**
     * 跟 {@link android.view.animation.AnticipateInterpolator}有点类似,
     * 但是会冲过终点然后再回来.
     * @return
     */
    public static AnticipateOvershootInterpolator anticipateOvershoot() {
        return new AnticipateOvershootInterpolator();
    }

    /**
     *
     * @param cycles
     * @return 以正弦的频率的回来变化,可以用来实现上面的晃动的效果
     */
    public static CycleInterpolator cycle(int cycles) {
        return new CycleInterpolator(cycles);
    }

    public static OvershootInterpolator overshoot() {
        return new OvershootInterpolator();
    }

    /**
     * 用来实现回弹效果
     * @param tension 松紧度
     * @return
     */
    public static OvershootInterpolator overshoot(float tension) {
        return new OvershootInterpolator(tension);
    }

}
