package xiyili.compat;

import android.os.Build;

/**
 * Created by banxi on 14-4-29.
 */
public class Env {
    public static final boolean hasGB = Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    public static final boolean hasICS = Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    public static final boolean hasJB = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    public static final boolean hasKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
}
