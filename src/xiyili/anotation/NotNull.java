package xiyili.anotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 表明某方法可能返回不会返回Null
 * @author banxi
 *
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD,ElementType.PARAMETER})
public @interface NotNull {

}
